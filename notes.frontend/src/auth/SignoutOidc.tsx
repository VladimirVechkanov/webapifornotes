import { FC, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { signinRedirectCallback } from "./user-service"

const SingoutOidc: FC<{}> = () => {
    const history = useHistory();
    useEffect(() => {
        const signoutAsync = async () => {
            await signinRedirectCallback();
            history.push('/');
        }
        signoutAsync();
    }, [history]);
    return <div>Redirecting...</div>;
};

export default SingoutOidc;