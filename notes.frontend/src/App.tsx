import { FC, ReactElement } from 'react';
import logo from './logo.svg';
import './App.css';
import userManager, { loadUser, signinRedirect, signoutRedirect } from './auth/user-service';
import AuthProvider from './auth/auth-provider';
import SigninOidc from './auth/SigninOidc';
import SignoutOidc from './auth/SignoutOidc';
import NoteList from './notes/NoteList';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

const App: FC<{}> = (): ReactElement => {
  loadUser();
  return (
    <div className="App">
      <header className="App-header">
        <button onClick={() => signinRedirect()}>Login</button>
        <AuthProvider userManager={userManager}>
          <Router>
            <Switch>
              <Route exact path="/" component={NoteList} />
              <Route path="/signout-oidc" component={SignoutOidc} />
              <Route  path="/signin-oidc" component={SigninOidc} />
            </Switch>
          </Router>
        </AuthProvider>
      </header>
    </div>
  );
}

export default App;
