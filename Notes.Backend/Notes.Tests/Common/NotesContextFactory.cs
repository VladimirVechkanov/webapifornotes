﻿using Microsoft.EntityFrameworkCore;
using Notes.Domain;
using Notes.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notes.Tests.Common
{
    public static class NotesContextFactory
    {
        public static Guid UserAId = Guid.NewGuid();
        public static Guid UserBId = Guid.NewGuid();

        public static Guid NoteIdForDelete = Guid.NewGuid();
        public static Guid NoteIdForUpdate = Guid.NewGuid();

        public static NotesDbContext Create()
		{
            var options = new DbContextOptionsBuilder<NotesDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new NotesDbContext(options);
            context.Database.EnsureCreated();
            context.Notes.AddRange(
                new Note
				{
                    CreationDate = DateTime.Today,
                    Details = "Details1",
                    EditDate = null,
                    Id = Guid.Parse("5B39ED56-DB4F-4CAD-A285-1CC7070F4C00"),
                    Title = "Title1",
                    UserId = UserAId
				},
                new Note
				{
                    CreationDate = DateTime.Today,
                    Details = "Details2",
                    EditDate = null,
                    Id = Guid.Parse("2D59050D-7E82-4D3A-9946-318AFA130008"),
                    Title = "Title2",
                    UserId = UserBId
				},
                new Note
				{
                    CreationDate = DateTime.Today,
                    Details = "Details3",
                    EditDate = null,
                    Id = NoteIdForDelete,
                    Title = "Title3",
                    UserId = UserAId
				},
                new Note
				{
                    CreationDate = DateTime.Today,
                    Details = "Details3",
                    EditDate = null,
                    Id = NoteIdForUpdate,
                    Title = "Title4",
                    UserId = UserBId
				});
            context.SaveChanges();
            return context;
		}

        public static void Destroy(NotesDbContext context)
		{
            context.Database.EnsureDeleted();
            context.Dispose();
		}
    }
}
